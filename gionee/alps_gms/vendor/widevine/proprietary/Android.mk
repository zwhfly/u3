# widevine prebuilts only available for ARM
# To build this dir you must define BOARD_WIDEVINE_OEMCRYPTO_LEVEL in the board config.
ifdef BOARD_WIDEVINE_OEMCRYPTO_LEVEL
ifneq ($(filter arm x86,$(TARGET_ARCH)),)

include $(call all-subdir-makefiles)
commonSharedLibraries :=libdiag \
    libdrmdiag \
    libutils \
    libdl \
    libcutils \
    liblog \
    libQSEEComAPI \
    libcrypto
######################################################################### 
# libwvm.so 
include $(CLEAR_VARS)
include $(TOP)/vendor/widevine/proprietary/wvm/wvm-core.mk
LOCAL_MODULE := libwvm
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_LIBRARIES += liboemcrypto
LOCAL_PRELINK_MODULE:= false
LOCAL_SHARED_LIBRARIES += $(commonSharedLibraries)
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_SHARED_LIBRARIES)
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY) 


######################################################################### 
# libdrmwvmplugin.so 
include $(CLEAR_VARS)
include $(TOP)/vendor/widevine/proprietary/drmwvmplugin/plugin-core.mk
LOCAL_MODULE := libdrmwvmplugin
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_LIBRARIES += liboemcrypto
LOCAL_SHARED_LIBRARIES += $(commonSharedLibraries)
LOCAL_PRELINK_MODULE:= false
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_SHARED_LIBRARIES)/drm
LOCAL_PROPRIETARY_MODULE := true

include $(BUILD_SHARED_LIBRARY)


######################################################################### 
# libdrmdecrypt.so 
include $(CLEAR_VARS)
include $(TOP)/vendor/widevine/proprietary/cryptoPlugin/decrypt-core.mk
LOCAL_C_INCLUDES := $(TOP)/frameworks/native/include/media/hardware \
    $(TOP)/vendor/widevine/proprietary/cryptoPlugin
LOCAL_MODULE := libdrmdecrypt
LOCAL_MODULE_TAGS := debug
LOCAL_STATIC_LIBRARIES += liboemcrypto
LOCAL_SHARED_LIBRARIES += $(commonSharedLibraries) \
    libstagefright_foundation
LOCAL_PRELINK_MODULE:= false
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_SHARED_LIBRARIES)
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)
endif # TARGET_ARCH == arm, x86
endif # BOARD_WIDEVINE_OEMCRYPTO_LEVEL
