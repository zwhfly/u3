#ifndef TouchlessA3D_AOEWNFOAWNFE
#define TouchlessA3D_AOEWNFOAWNFE

#include "touchless_a3d/errors.h"
#include "touchless_a3d/parameters.h"
#include "touchless_a3d/gesture.h"
#include "touchless_a3d/gesture_listener.h"
#include "touchless_a3d/motion.h"
#include "touchless_a3d/motion_listener.h"
#include "touchless_a3d/state_listener.h"
#include "touchless_a3d/engine.h"

#include "touchless_a3d/gestures/open_hand_presence.h"
#include "touchless_a3d/gestures/fist_presence.h"
#include "touchless_a3d/gestures/face_presence.h"
#include "touchless_a3d/gestures/thumbs_up_presence.h"
#include "touchless_a3d/gestures/swipe.h"
#include "touchless_a3d/gestures/v_sign_presence.h"

#endif
