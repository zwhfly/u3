/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for face process.
 *
 * Author : zhuangxiaojian
 * Email  : zhuangxj@gionee.com
 * Date   : 2014-10-28
 *
 *************************************************************************************/

#define LOG_TAG "ArcSoftFaceProcess"
#include <android/log.h>
#include <string.h>

#include<stdio.h>

#include "ArcSoftFaceProcess.h"

#define STYLE_PATH "system/etc/gn_camera_feature/arcsoft/style/style.cng"

namespace android {
ArcSoftFaceProcess::ArcSoftFaceProcess()
	: mInitialized(false)
	, mGNFaceProcessFeature(GN_CAMERA_FACE_FEATURE_NONE)
{
	mPreviewBeautyShot = new CBeautyShot();
	if (mPreviewBeautyShot == NULL) {
		PRINTD("Failed to new mPreviewBeautyShot");
	}

	mVideoBeautyShot = new CBeautyShot();
	if (mVideoBeautyShot == NULL) {
		PRINTD("Failed to new mVideoBeautyShot");
	}

	mImageBeautyShot = new CBeautyShot();
	if (mImageBeautyShot == NULL) {
		PRINTD("Failed to new mImageBeautyShot");
	}
}

ArcSoftFaceProcess::~ArcSoftFaceProcess()
{
	if (mPreviewBeautyShot != NULL) {
		delete mPreviewBeautyShot;
		mPreviewBeautyShot = NULL;
	}

	if (mVideoBeautyShot != NULL) {
		delete mVideoBeautyShot;
		mVideoBeautyShot = NULL;
	}

	if (mImageBeautyShot!= NULL) {
		delete mImageBeautyShot;
		mImageBeautyShot = NULL;
	}
}

ArcSoftFaceProcess* 
ArcSoftFaceProcess::mArcSoftFaceProcess = NULL;

ArcSoftFaceProcess*
ArcSoftFaceProcess::
getInstance()
{
	if (mArcSoftFaceProcess == NULL) {
		mArcSoftFaceProcess = new ArcSoftFaceProcess();
	}

	return mArcSoftFaceProcess;
}

int32
ArcSoftFaceProcess::
init()
{
	MRESULT res = 0;

	return res;
}

void
ArcSoftFaceProcess::
deinit()
{
	if (mInitialized) {
		if (mPreviewBeautyShot != NULL) {
			mPreviewBeautyShot->Uninit();
		}
		
		if (mVideoBeautyShot != NULL) {
			mVideoBeautyShot->Uninit();
		}

		if (mImageBeautyShot != NULL) {
			mImageBeautyShot->Uninit();
		}
		
		mInitialized = false;
	}
}

int32
ArcSoftFaceProcess::
enableFaceProcess(int featureMask)
{
	MRESULT res = 0;

	int mask = GN_CAMERA_FACE_FEATURE_FACEBEAUTY | GN_CAMERA_FACE_FEATURE_AGEGENDER_DETECTION;
	
	if ((featureMask & mask) == 0) {
		return res;
	}

	mGNFaceProcessFeature |= featureMask;

	if (!mInitialized) {
		res = mPreviewBeautyShot->Init(DataTypeVideo, false);
		if (res != 0) {
			PRINTE("Failed to initialized mPreviewBeautyShot [%ld].", res);
			return res;
		}
		
		res = mVideoBeautyShot->Init(DataTypeVideo, false);
		if (res != 0) {
			PRINTE("Failed to initialized mVideoBeautyShot [%ld].", res);
			return res;
		}

		res = mImageBeautyShot->Init(DataTypeImage, false);
		if (res != 0) {
			PRINTE("Failed to initialized mImageBeautyShot [%ld]", res);
			return res;
		}
		
		mInitialized = true;

		ReadFile(mBuffer, &mSize);

		//res = mPreviewBeautyShot->LoadStyle(mBuffer, mSize);
		//if (res != 0) {
		//	PRINTD("Failed to mPreviewBeautyShot LoadStyle[#%ld]", res);
		//}

		res = mImageBeautyShot->LoadStyle(mBuffer, mSize);
		if (res != 0) {
			PRINTD("Failed to mImageBeautyShot LoadStyle[#%ld]", res);
		}

		PRINTD("%s initialized end", __func__);
	}

	return res;
}

int32
ArcSoftFaceProcess::
disableFaceProcess(int featureMask)
{
	MRESULT res = 0;

	int mask = GN_CAMERA_FACE_FEATURE_FACEBEAUTY
		  | GN_CAMERA_FACE_FEATURE_AGEGENDER_DETECTION;

	if ((featureMask & mask) == 0) {
		return res;
	}

	mGNFaceProcessFeature &= ~featureMask;
	
	if (((mGNFaceProcessFeature & mask) == 0) && mInitialized) {
		if (mPreviewBeautyShot != NULL) {
			mPreviewBeautyShot->Uninit();
		}

		if (mVideoBeautyShot != NULL) {
			mVideoBeautyShot->Uninit();
		}

		if (mImageBeautyShot != NULL) {
			mImageBeautyShot->Uninit();
		}
		
		mInitialized = false;
	}

	return res;
}

int32
ArcSoftFaceProcess::
processFaceBeauty(LPASVLOFFSCREEN imgSrc, BEAUTY_PARAM* param, MLong index, GNDataType dataType)
{
	MRESULT res = 0;

	LPASVLOFFSCREEN pImgDst = imgSrc;
	PRINTD("%s mSize = %d", __func__, mSize);
	switch(dataType) {
		case GN_DATA_TYPE_PREVIEW:
			if (mPreviewBeautyShot != NULL) {
				res = mPreviewBeautyShot->Process(imgSrc, pImgDst, index, param);
				if (res != 0) {
					PRINTD("Failed to process Face Beauty preview[#%ld]", res);
				}
			}
			break;
		case GN_DATA_TYPE_VIDEO:
			if (mVideoBeautyShot != NULL) {
				res = mVideoBeautyShot->Process(imgSrc, pImgDst, index, param);
				if (res != 0) {
					PRINTD("Failed to process Face Beauty video[#%ld]", res);
				}
			}
			break;
		case GN_DATA_TYPE_SNAPSHOT:
			if (mImageBeautyShot!= NULL) {
				res = mImageBeautyShot->Process(imgSrc, pImgDst, index, param);
				if (res != 0) {
					PRINTD("Failed to process Face Beauty snapshot[#%ld]", res);
				}
			}
			break;
		default:
			PRINTD("%s Invalid Data Type", __func__);
			break;
	}

	return res;
}

int32
ArcSoftFaceProcess::
faceDetection(LPASVLOFFSCREEN param, PAGE_GENDER_INFO** ageGenderInfo, MLong* faceNum, GNDataType dataType)
{
	MRESULT res = 0;

	switch(dataType) {
		case GN_DATA_TYPE_PREVIEW:
			if (mPreviewBeautyShot != NULL) {
				res = mPreviewBeautyShot->Detect(param, ageGenderInfo, faceNum);
				if (res != 0) {
					PRINTD("Preview Detect failed [#%ld]", res);
				}
			}
			break;
		case GN_DATA_TYPE_VIDEO:
			if (mVideoBeautyShot != NULL) {
				res = mVideoBeautyShot->Detect(param, ageGenderInfo, faceNum);
				if (res != 0) {
					PRINTD("Video Detect failed [#%ld]", res);
				}
			}
			break;
		case GN_DATA_TYPE_SNAPSHOT:
			if (mImageBeautyShot != NULL) {
				res = mImageBeautyShot->Detect(param, ageGenderInfo, faceNum);
				if (res != 0) {
					PRINTD("Image Detect failed [#%ld]", res);
				}
			}
			break;
		default:
			PRINTD("%s Invalid Data Type", __func__);
			break;
	}
	
	return res;
}

void 
ArcSoftFaceProcess::
ReadFile(MByte* buffer, int* readSize)
{
    FILE *fp = fopen(STYLE_PATH, "r");
    if (NULL == fp) {
		PRINTD("%s fopen faile", __func__);
        return;
    }

	long curStep = ftell(fp);

	fseek(fp,0L,SEEK_END);

	int fileSize = ftell(fp);

	fseek(fp,curStep,SEEK_SET);

	PRINTD("%s fileSize = %d", __func__, fileSize);

    *readSize = fread(buffer, 1, fileSize, fp);
	
    PRINTD("fread %d", *readSize);

    fclose(fp);
}

};
