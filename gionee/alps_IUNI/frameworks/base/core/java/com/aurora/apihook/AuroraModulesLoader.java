package com.aurora.apihook;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.aurora.apihook.XC_MethodHook.MethodHookParam;




import android.os.Build;
import android.text.GetChars;
import android.text.TextUtils;
import android.util.Log;
import dalvik.system.PathClassLoader;
public class AuroraModulesLoader {
	
	private static final int KITKAT = 19;
	
	private static final int JELLY_BEAN_MR1 = 17;
	
	private static final int JELLY_BEAN_MR2 = 18;
	
	private static final String TAG="init";
	
	public static final String BEFORE_PREFIX = "before_";
	
	public static final String AFTER_PREFIX = "after_";
	/**
	 * target apk,all of logic for aurora component are here
	 */
	private static final String APK_PATH="system/app/Aurora_Hook.apk";
	
	private static final String MODULE_READER_NAME="com.aurora.apihook.ComponentReader";
	/*
	 * 和register.xml中的标签一一对应
	 */
   private static final String COMPONENT_TAG_NAME="component";
	
	private static final String COMPONENT_TARGET_METHOD="targetMethod";
	
	private static final String COMPONENT_TARGET_CLASS_NAME="targetClassName";
	
	private static final String COMPONENT_IMP_CLASS_NAME="impClassName";
	
	private static final String COMPONENT_METHOD_NAME="name";
	
	private static final String COMPONENT_INVOKE_TYPE="invokeType";
	
	private static final String COMPONENT_TARGET_METHOD_ARGS="methodArg";
	
	private static final String COMPONENT_METHOD_IS_CONSTRUCTOR="constructor";
	
	private static final String COMPONENT_METHOD_MIN_VERSION="minSdkVersion";
	
	private static final String COMPONENT_METHOD_MAX_VERSION="maxSdkVersion";
	
	private static final boolean DEBUG_ABLE = false;
	
	private static final ClassLoader BOOTCLASSLOADER = ClassLoader.getSystemClassLoader();
	
	private static final HashMap<String, Object> mModulesClasses = new HashMap<String, Object>();
	
	private static StringBuffer mBuffer = new StringBuffer();
	/**
	 * invoked before original method
	 */
	public static final int BEFORE = 0;
	/**
	 * invoked after original method
	 */
	public static final int AFTER = 1;
	
	public static  List<String> mHookClasses ;
	private static  ClassLoader clzLoader;
	private static HashMap<String, String> methodsMap = new HashMap<String, String>();
	private static Class<?> modulesReaderClz; 
	private static List<Object> mObjects = new ArrayList<Object>();
	
	private static HashMap<String, Component> mObjectMap = new HashMap<String, Component>();
	
	private static List<String> mImpClass = new ArrayList<String>();
//	private StringBuffer 
	/**
	 * all of files need to change will added
	 * 
	 * com.android.internal.app.AlertController
	 * com.android.internal.app.AlertController.AlertParams
	 * com.android.server.AlarmManagerService
	 * com.android.internal.policy.impl.PhoneWindowManager
	 * android.widget.Toast.TN#handleShow
	 * com.android.server.am.ActivityManagerService#startActivityAsUser
	 * android.content.ComponentName
	 * android.app.WallpaperManager
	 * com.android.server.WallpaperManagerService
	 * android.widget.TextView
	 * android.view.ViewRootImpl.EarlyPostImeInputStage
	 * com.android.internal.telephony.DefaultPhoneNotifier
	 * android.view.View
	 * version less than 4.4
	 * com.android.internal.telephony.SMSDispatcher
	 * version more than 4.4
	 * com.android.internal.telephony.SmsApplication
	 */
	public static void init(){
//		param.invoke("com.aurora.apihook.phonewindowmanager.DecorViewHook#"
//    			+ "com.android.internal.policy.impl.PhoneWindow.DecorView#fitSystemWindows");
		methodsMap.put("com.aurora.apihook.phonewindowmanager.DecorViewHook#"
    			+ "com.android.internal.policy.impl.PhoneWindow.DecorView#fitSystemWindows","fitSystemWindows");
		
		methodsMap.put("com.aurora.apihook.activity.ActivityHook#android.app.Activity#onResume", "onResume");
		methodsMap.put("com.aurora.apihook.activity.ActivityHook#android.app.Activity#onWindowFocusChanged", "onWindowFocusChanged");
		/**
		 * add for dialog
		 */
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#installContent", "installContent");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#setTitle", "setTitle");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#setCustomTitle", "setCustomTitle");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#setMessage", "setMessage");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#setView", "setView");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#setButton", "setButton");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#setIcon", "setIcon");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#getIconAttributeResId", "getIconAttributeResId");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#setInverseBackgroundForced", "setInverseBackgroundForced");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#getListView", "getListView");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#getButton", "getButton");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#onKeyDown", "onKeyDown");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#onKeyUp", "onKeyUp");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#getListView", "getListView");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController.AlertParams#apply", "apply");
		
		methodsMap.put("com.aurora.apihook.dialog.DialogHook#" +
				"com.android.internal.app.AlertController#AlertController", "AlertController");
		
		/**
		 * add for alarmmanager
		 */
		if(getCurrentOsVersion() <KITKAT){
		methodsMap.put("com.aurora.apihook.alarmmanager.AlarmManagerServiceHook#" +
				"com.android.server.AlarmManagerService#setRepeating", "setRepeating");
		}else{
		methodsMap.put("com.aurora.apihook.alarmmanager.AlarmManagerServiceHook#" +
				"com.android.server.AlarmManagerService#setImplLocked", "setImplLocked");
		}
		
		/**s
		 * add for phoneWindowManager
		 */
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHook#" +
				"com.android.internal.policy.impl.PhoneWindowManager#checkAddPermission", "checkAddPermission");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHook#" +
				"com.android.internal.policy.impl.PhoneWindowManager#showGlobalActionsDialog", "showGlobalActionsDialog");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHook#" +
				"com.android.internal.policy.impl.PhoneWindowManager#interceptKeyBeforeQueueing", "interceptKeyBeforeQueueing");
		
		
		/**
		 * add for Toast
		 */
		methodsMap.put("com.aurora.apihook.toasthook.ToastHook#" +
				"android.widget.Toast.TN#handleShow", "handleShow");
		
		/**
		 * add for ActivitymanagerService
		 */
		methodsMap.put("com.aurora.apihook.resolver.ChooserActivityHook#" +
				"com.android.server.am.ActivityManagerService#startActivityAsUser", "startActivityAsUser");
		
		
		/**
		 * add for ComponentName
		 */
		methodsMap.put("com.aurora.apihook.resolver.ResolverActivityHook#" +
				"android.content.ComponentName#ComponentName", "ComponentName");
		
		/**
		 * add for WallpaperManager
		 */
		methodsMap.put("com.aurora.apihook.wallpaper.WallpaperManagerHook#" +
				"android.app.WallpaperManager#setBitmap", "setBitmap");
		
		
		
		/**
		 * add for WallpaperManagerService
		 */
		methodsMap.put("com.aurora.apihook.wallpaper.WallpaperManagerServiceHook#" +
				"com.android.server.WallpaperManagerService#loadSettingsLocked", "loadSettingsLocked");
		
		
		/**
		 * add for TextView
		 */
		methodsMap.put("com.aurora.apihook.textview.TextViewHook#" +
				"android.widget.TextView#setTypefaceFromAttrs", "setTypefaceFromAttrs");
//		methodsMap.put("android.text.TextViewHook#" +
//				"android.widget.TextView#TextView", "TextView");
		
		/**
		 * add for PhoneWindowManager
		 */
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHook2#" +
				"com.android.internal.policy.impl.PhoneWindowManager#init", "init");
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHook2#" +
				"com.android.internal.policy.impl.PhoneWindowManager#updateSettings", "updateSettings");
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHook2#" +
				"com.android.internal.policy.impl.PhoneWindowManager#prepareAddWindowLw", "prepareAddWindowLw");
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHook2#" +
				"com.android.internal.policy.impl.PhoneWindowManager#removeWindowLw", "removeWindowLw");
        	// Aurora <Felix.Duan> <2015-1-16> <BEGIN> AuroraHook "has_navigation_bar" to Settings.System
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHook2#" +
				"com.android.internal.policy.impl.PhoneWindowManager#setInitialDisplaySize", "setInitialDisplaySize");
		// Aurora <Felix.Duan> <2015-1-16> <END> AuroraHook "has_navigation_bar" to Settings.System

		/**
		 * add for keyguard
		 * 
		 */
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#systemReady", "systemReady");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#screenTurningOn", "screenTurningOn");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#screenTurnedOff", "screenTurnedOff");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#keyguardIsShowingTq", "keyguardIsShowingTq");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#inKeyguardRestrictedKeyInputMode", "inKeyguardRestrictedKeyInputMode");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#dismissKeyguardLw", "dismissKeyguardLw");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#launchHomeFromHotKey", "launchHomeFromHotKey");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#finishPostLayoutPolicyLw", "finishPostLayoutPolicyLw");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#isKeyguardSecure", "isKeyguardSecure");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#exitKeyguardSecurely", "exitKeyguardSecurely");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#enableKeyguard", "enableKeyguard");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#notifyLidSwitchChanged", "notifyLidSwitchChanged");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#interceptMotionBeforeQueueingWhenScreenOff", "interceptMotionBeforeQueueingWhenScreenOff");
		
		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
 				"com.android.internal.policy.impl.PhoneWindowManager#setCurrentUserLw", "setCurrentUserLw");
		
		// Aurora <liugj> <2015-1-31> <BEGIN> for bug-11317
//		methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
// 				"com.android.internal.policy.impl.PhoneWindowManager#removeWindowLw", "removeWindowLw");
		// Aurora <liugj> <2015-1-31> <END> for bug-11317
		
		// Aurora <liugj> <2015-2-4> <BEGIN> for bug-11183
		methodsMap.put("com.aurora.apihook.keyguard.LockSettingsServiceHook#" +
 				"com.android.server.LockSettingsService#checkPassword", "checkPassword");
		// Aurora <liugj> <2015-2-4> <END> for bug-11183

		if(getCurrentOsVersion() <KITKAT){
			methodsMap.put("com.aurora.apihook.phonewindowmanger.PhoneWindowManagerHookForKeyguard#" +
	 				"com.android.internal.policy.impl.PhoneWindowManager#showAssistant", "showAssistant");
		}
		
		/**
		 * add for EarlyPostImeInputStage
		 */
		methodsMap.put("com.aurora.apihook.view.EarlyPostImeInputStageHook#" +
				"android.view.ViewRootImpl.EarlyPostImeInputStage#processPointerEvent", "processPointerEvent");
		
		
		methodsMap.put("com.aurora.apihook.telephony.DefaultPhoneNotifierHook#" +
				"com.android.internal.telephony.DefaultPhoneNotifier#notifyPhoneState", "notifyPhoneState");
		
		/**
		 * add for view
		 */
		methodsMap.put("com.aurora.apihook.adblock.AdblockHook#" +
				"android.view.View#View", "View");
		
		methodsMap.put("com.aurora.apihook.adblock.AdblockHook#" +
				"android.view.View#setVisibility", "setVisibility");
		
		methodsMap.put("com.aurora.apihook.adblock.AdblockHook_ex#" +
				"android.view.View#View", "View");
		
		/**
		 * add for SMSDispatcher
		 */
		if(getCurrentOsVersion() < KITKAT){
		methodsMap.put("com.aurora.apihook.telephony.SMSDispatcherHook#" +
				"com.android.internal.telephony.SMSDispatcher#dispatch", "dispatch");
		}
		
		/**
		 * add for AudioService
		 */
		methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
				"android.media.AudioService#AudioService", "AudioService");
		
		methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
				"android.media.AudioService#sendVolumeUpdate", "sendVolumeUpdate");
		
		methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
				"android.media.AudioService#sendMasterVolumeUpdate", "sendMasterVolumeUpdate");
		
		methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
				"android.media.AudioService#sendMasterMuteUpdate", "sendMasterMuteUpdate");
		
		methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
				"android.media.AudioService#onCheckMusicActive", "onCheckMusicActive");
		
		methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
				"android.media.AudioService#adjustStreamVolume", "adjustStreamVolume");
		
		int currentOsVersion = getCurrentOsVersion();
		if(currentOsVersion == /*4.2*/ JELLY_BEAN_MR1){
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
					"android.media.AudioService#checkSafeMediaVolume", "checkSafeMediaVolume");
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
			"android.media.AudioService#onReevaluateRemote", "onReevaluateRemote");
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
					"android.media.AudioService#onNewPlaybackInfoForRcc", "onNewPlaybackInfoForRcc");
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
					"android.media.AudioService#adjustRemoteVolume", "adjustRemoteVolume");
			
		}else if(currentOsVersion == /*4.3*/JELLY_BEAN_MR2){
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
					"android.media.AudioService#handleConfigurationChanged", "handleConfigurationChanged");
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
					"android.media.AudioService#setStreamVolume", "setStreamVolume");
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
			"android.media.AudioService#onNewPlaybackInfoForRcc", "onNewPlaybackInfoForRcc");
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
			"android.media.AudioService#adjustRemoteVolume", "adjustRemoteVolume");
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
			"android.media.AudioService#onReevaluateRemote", "onReevaluateRemote");
		}else if(currentOsVersion ==  /*4.4*/KITKAT){
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
					"android.media.AudioService#handleConfigurationChanged", "handleConfigurationChanged");
			
			methodsMap.put("com.aurora.apihook.volumepanel.VolumePanelHookForAudioService#" +
					"android.media.AudioService#setStreamVolume", "setStreamVolume");
		}
		
		/**
		 * add for AssetManager
		 */
		methodsMap.put("com.aurora.apihook.assetmanager.AssetManagerHook#" +
				"android.content.res.AssetManager#AssetManager", "AssetManager");
		
		/**
		 * add for PackageManagerService
		 */
		methodsMap.put("com.aurora.apihook.pm.PackageManagerService#" +
				"com.android.server.pm.PackageManagerService#compareSignatures", "compareSignatures");
		
		methodsMap.put("com.aurora.apihook.pms.PmsHook#" +
				"com.android.server.pm.PackageManagerService#getPackageInfo", "getPackageInfo");
		
		methodsMap.put("com.aurora.apihook.pms.PmsHook#" +
				"com.android.server.pm.PackageManagerService#getApplicationInfo", "getApplicationInfo");
		
		
		methodsMap.put("com.aurora.apihook.pms.PmsHook#" +
				"com.android.server.pm.PackageManagerService#getInstalledPackages", "getInstalledPackages");
		
		
		methodsMap.put("com.aurora.apihook.pms.PmsHook#" +
				"com.android.server.pm.PackageManagerService#getInstalledApplications", "getInstalledApplications");
		
		
		/**
		 * add for SmsApplication,since KITKAT(version 4.4)
		 */
		if(getCurrentOsVersion() >= KITKAT){
			methodsMap.put("com.aurora.apihook.telephony.SmsApplicationHook#" +
					"com.android.internal.telephony.SmsApplication#getDefaultSmsApplication", "getDefaultSmsApplication");
			
			methodsMap.put("com.aurora.apihook.telephony.SmsApplicationHook#" +
					"com.android.internal.telephony.SmsApplication#getDefaultMmsApplication", "getDefaultMmsApplication");
			
			methodsMap.put("com.aurora.apihook.telephony.SmsApplicationHook#" +
					"com.android.internal.telephony.SmsApplication#getDefaultRespondViaMessageApplication", "getDefaultRespondViaMessageApplication");
			
			methodsMap.put("com.aurora.apihook.telephony.SmsApplicationHook#" +
					"com.android.internal.telephony.SmsApplication#getDefaultSendToApplication", "getDefaultSendToApplication");
			
			methodsMap.put("com.aurora.apihook.telephony.SmsApplicationHook#" +
					"com.android.internal.telephony.SmsApplication#shouldWriteMessageForPackage", "shouldWriteMessageForPackage");
			
		}
		
		

                /*
                 * add for auto brightness
                 */
		
                methodsMap.put("com.aurora.apihook.pms.PowerManagerServiceHook#"+
                             "com.android.server.power.PowerManagerService#updateDisplayPowerStateLocked","updateDisplayPowerStateLocked");

                methodsMap.put("com.aurora.apihook.dpc.DisplayPowerControllerHook#"
                           + "com.android.server.power.DisplayPowerController#DisplayPowerController","DisplayPowerController");
               
                methodsMap.put("com.aurora.apihook.dpc.DisplayPowerControllerHook#"
                           + "com.android.server.power.DisplayPowerController#updateAutoBrightness","updateAutoBrightness");
               
                 methodsMap.put("com.aurora.apihook.dpc.DisplayPowerControllerHook#"
                           + "com.android.server.power.DisplayPowerController#setLightSensorEnabled","setLightSensorEnabled");

                 methodsMap.put("com.aurora.apihook.dpc.DisplayPowerControllerHook#"
                           + "com.android.server.power.DisplayPowerController#updatePowerState","updatePowerState");               
		
                 methodsMap.put("com.aurora.apihook.wifiservice.WifiServiceHook#"
                           + "com.android.server.wifi.WifiService#WifiService","WifiService");    

                 methodsMap.put("com.aurora.apihook.usbdevicemanager.UsbDeviceManagerHook#"
                           + "com.android.server.usb.UsbDeviceManager.UsbHandler#updateUsbNotification","updateUsbNotification");   
                           
                  methodsMap.put("com.aurora.apihook.usbdevicemanager.UsbDeviceManagerHook#"
                           + "com.android.server.usb.UsbDeviceManager.UsbHandler#updateAdbNotification","updateAdbNotification");   
    /**
		 * add for notification xml
		 */
		methodsMap.put("com.aurora.apihook.app.NotificationHook#" +
					"com.android.app.Notification.Builder#applyStandardTemplate", "applyStandardTemplate");
					
		methodsMap.put("com.aurora.apihook.app.NotificationHook#" +
					"com.android.app.Notification.Builder#applyStandardTemplateWithActions", "applyStandardTemplateWithActions");
						
		methodsMap.put("com.aurora.apihook.app.NotificationHook#" +
					"com.android.app.Notification.Builder#makeContentView", "makeContentView");
					
		methodsMap.put("com.aurora.apihook.app.NotificationHook#" +
					"com.android.app.Notification.Builder#makeTickerView", "makeTickerView");
					
		methodsMap.put("com.aurora.apihook.app.NotificationHook#" +
					"com.android.app.Notification.Builder#makeBigContentView", "makeBigContentView");
					
		methodsMap.put("com.aurora.apihook.app.NotificationHook#" +
					"com.android.app.Notification.Builder#generateActionButton", "generateActionButton");
					
		methodsMap.put("com.aurora.apihook.app.NotificationHook#" +
					"com.android.app.Notification.Style#getStandardView", "getStandardView");
					
		methodsMap.put("com.aurora.apihook.app.NotificationBigPictureStyleHook#" +
					"com.android.app.Notification.BigPictureStyle#makeBigContentView", "makeBigContentView");
					
		methodsMap.put("com.aurora.apihook.app.NotificationBigTextStyleHook#" +
					"com.android.app.Notification.BigTextStyle#makeBigContentView", "makeBigContentView");
					
		methodsMap.put("com.aurora.apihook.app.NotificationInboxStyleHook#" +
					"com.android.app.Notification.InboxStyle#makeBigContentView", "makeBigContentView");  
		/**
		 * add for NotificationManagerService
		 */	
		methodsMap.put("com.aurora.apihook.server.NotificationManagerServiceHook#" +
					"com.android.server.NotificationManagerService#isUidSystem", "isUidSystem");     

		/**
		 * just load one time
		 */
		try{
			long start = System.currentTimeMillis();
			clzLoader = new PathClassLoader(APK_PATH,BOOTCLASSLOADER);
		    Set<String> keySet = methodsMap.keySet();
		    Iterator<String> iterator = keySet.iterator();
		    while(iterator.hasNext()){
		    	String key = (String) iterator.next();
		    	String[] sp = key.split("#");
		    	Class<?> Clz = clzLoader.loadClass(sp[0]);
		    	if(Clz != null){
		    		
		    		if(containt(sp[0])){
		    			continue;
		    		}
		    		log(  "ClassName**********:"+sp[0]+"      Clz != null");
		    		Object instance = Clz.newInstance();
					Component comp = new Component();
					comp.classObj = instance;
					comp.key = key;
					comp.name = sp[0];
					mObjectMap.put(key, comp);
		    	}
		    }
		    long end = System.currentTimeMillis();
		    log( "totalTime:"+(end - start)+" size:"+mObjectMap.size());
			
			}catch(Exception e){
 e.printStackTrace();
				log( "init Exception*********************** ");
			}
	}
	
	private static  int getCurrentOsVersion(){
		return android.os.Build.VERSION.SDK_INT;
	}
	
	private static boolean containt(String com){
		Set<String> keySet = mObjectMap.keySet();
		Iterator<String> in = keySet.iterator();
		while (in.hasNext()) {
			String key = (String) in.next();
			Component comOld = mObjectMap.get(key);
			if(comOld.name.equals(com)){
				return true;
			}
			
		}
		return false;
	}
	
	private static boolean mIsLoaded = false;
	static class Component{
		Object classObj;
		String key;
		String name;
		
		public boolean equalsCom(Component obj) {
			// TODO Auto-generated method stubobj
			return obj.name.equals(name);
		}
	}
	
	/**
	 * create a MethodHookParam to save object's data
	 * @param obj
	 * @param args
	 * @param invokeType
	 * @return
	 */
	public static MethodHookParam createParams(Object obj,Object[] args,int invokeType){
		MethodHookParam param = new MethodHookParam();
		param.thisObject = obj;
		param.args = args;
		param.invokeType = invokeType;
		return param;
	}
	
	/**
	 * invoke method that define in Aurora_Hook.apk 
	 * @param key
	 * @param param
	 */
	public static void invoke(String key,MethodHookParam param){
		long startTime = System.currentTimeMillis();
		if(!mIsLoaded||TextUtils.isEmpty(key)|| !key.contains("#")){
			return;
		}
		Component component = null;
//		synchronized (mObjectMap) {
		try{
			Set<String> keys = mObjectMap.keySet();
			Iterator<String> in = keys.iterator();
			String orgKey = key.split("#")[0]; 
			while (in.hasNext()) {
				String keyInternal = (String)in.next();
				String className = keyInternal.split("#")[0];
				if(orgKey.equals(className)){
					component = mObjectMap.get(keyInternal);
				}
				
			}
			
			if(component == null){
				return;
			}
			log( "mComponent != null********"+key);
			Class<?> clz = component.classObj.getClass();
			
			String methodName = key.split("#")[2];
			String prefix = "";
			if(param.invokeType == BEFORE){
				prefix = BEFORE_PREFIX;
			}else{
				prefix = AFTER_PREFIX;
			}
			if(clz != null){
				log(  "methodName:"+clz.getName()+"#"+prefix+methodName);
				Method method = clz.getDeclaredMethod(prefix+methodName, MethodHookParam.class);
				if(method != null){
					method.setAccessible(true);
					method.invoke(component.classObj, param);
				}
			}
			
			long endTime = System.currentTimeMillis();
			log(  "invoke Time:"+(endTime - startTime));
		}catch(Exception e){
			log( "load Exception");
			e.printStackTrace();
			log( "load Exception 222");
		}
		
//		}
	}
	private static void log(String msg){
		if(DEBUG_ABLE){
			Log.i(TAG, msg);
		}
	}
	public static void loadModules(){
		init();
		mIsLoaded = true;
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
