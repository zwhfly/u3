# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := libaurorascanner_jni
LOCAL_SRC_FILES :=  \
	native/AuroraMediaScanner.cpp \
	native/AuroraMetaMediaScanner.cpp  \
	native/AuroraMediaScannerClient.cpp  \
	com_android_aurora_AuroraScanner.cpp

LOCAL_LDLIBS := -lc -lm -lstdc++ -ldl -llog -lpthread

LOCAL_SHARED_LIBRARIES := \
    libandroid_runtime \
    libbinder \
    libnativehelper \
    libutils  \
    libicuuc  \
    libcutils \
    libaudioutils  \
    libmedia  \
    libsonivox
    	
	
LOCAL_C_INCLUDES += \
    external/jhead \
    $(PV_INCLUDES) \
    $(JNI_H_INCLUDE) \
    external/icu4c/common \
    frameworks/av/media/libmedia \
    $(call include-path-for, corecg graphics) \
    $(call include-path-for, audio-utils)

include $(BUILD_SHARED_LIBRARY)
 
